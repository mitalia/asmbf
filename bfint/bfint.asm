    org 100h

datastart  equ 1000h
datasize   equ 30000
codestart  equ 9000h
codesize   equ 6fffh

start:
    ; cleanup the data and code cells
    xor al,al
    mov di,datastart
    cld
    mov cx,0ffffh-datastart
    rep stosb
    ; read the input file
    xor bx,bx
    mov bl,byte[80h]
    mov byte[bx+81h],0
    mov dx,82h
    mov ah,3dh
    int 21h
    int 3h
    mov bx,ax
    mov ah,3fh
    mov cx,codesize
    mov dx,codestart
    int 21h
    ; di: data pointer
    mov di,datastart
    ; si: code pointer
    mov si,codestart-1
l:
    inc si
    mov al,byte[si]
    cmp al,'<'
    jne l1
    dec di
    jmp l
l1:
    cmp al,'>'
    jne l2
    inc di
    jmp l
l2:
    cmp al,'+'
    jne l3
    inc byte[di]
    jmp l
l3:
    cmp al,'-'
    jne l4
    dec byte[di]
    jmp l
l4:
    cmp al,'.'
    jne l5
    mov dl,byte[di]
    mov ah,2h
    int 21h
    jmp l
l5:
    cmp al,','
    jne l6
    mov ah,1h
    int 21h
    mov byte[di],al
    jmp l
l6:
    cmp al,'['
    jne l7
    cmp byte[di],0
    jne l
    call matchparen
    jmp l
l7:
    cmp al,']'
    jne l8
    cmp byte[di],0
    je l
    call matchparen
    jmp l
l8:
    cmp al,0
    jne l
    int 20h

    ; receives '[' or ']' in al, moves si accordingly
matchparen:
    xor bx,bx
    and ax,2
    jz setdec
    mov byte[mpl],46h
    jmp mplstart
setdec:
    mov byte[mpl],4eh
    jmp mplstart
mpl:
    dec si  ; this gets changed to inc above if necessary
    mov al,byte[si]
    cmp al,']'
    je hasbracket
    cmp al,'['
    jne mpl
hasbracket:
    and ax,2
mplstart:
    dec ax
    add bx,ax
    jnz mpl
    ret
